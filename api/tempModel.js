var mongoose = require('mongoose');
// Setup schema
var tempSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    value: {
        type: String,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Temp model
var Temp = module.exports = mongoose.model('temp', tempSchema);
module.exports.get = function (callback, limit) {
    Temp.find(callback).limit(limit);
}