var request = require("request");

var TEMP_MAX = 60;
// Import temp model
Temp = require('./tempModel');

// Handle index actions
exports.index = function (req, res) {
    Temp.get(function (err, temps) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        } else {
            var temperatures = [{name:'capteur_temp_1', data: [] }, {name: 'capteur_temp_2', data: []}];
            // console.log("----> Liste complete: ", temps);
            var yesterday = new Date();
            yesterday.setDate(yesterday.getDate() - 1);
            temps.forEach(elt => {
                //console.log("Elt: ", elt);
                nomCapteur = elt.name;
                valeurTemperature = parseFloat(elt.value);
                dateCorrecte = new Date(elt.create_date);
                dateCorrecte = dateCorrecte.setHours(dateCorrecte.getHours() + 1);
                //console.log("Index: ", temperatures[index]);
                if(dateCorrecte > yesterday && valeurTemperature < TEMP_MAX){
                    if(nomCapteur === 'capteur_temp_1') {
                        temperatures[0].data.push({y: valeurTemperature, x: dateCorrecte});
                    } else {
                        temperatures[1].data.push({y: valeurTemperature, x: dateCorrecte});
                    }
                }
            });
            
            res.json({
                status: "success",
                message: "Temps retrieved successfully",
                data: temperatures
            });
        }
    });
};
// Handle create temp actions
exports.new = function (req, res) {
    recupererSeuilAlerte(function(seuilAlerte) {
        console.log("****> Seuil dans temp: ", seuilAlerte);
        var temp = new Temp();
        temp.name = req.body.name;
        temp.value = req.body.value;

        // save the temp and check for errors
        temp.save(function (err) {
            if (err){
                res.json(err);
            } else {
                if(temp.value > seuilAlerte && temp.value < TEMP_MAX){
                    var options = { 
                        method: 'POST',
                        url: 'http://localhost:8080/api/email',
                        headers: { 
                            'Content-Type': 'application/json' 
                        },
                        body: { 
                            destinataires: 'amadou.niamadio@gmail.com',
                            value: 'Un mail d\'alerte a été envoyé aux destinataires ',
                            nomCapteur: temp.name,
                            valeurTemperature: temp.value
                        },
                        json: true 
                    };

                    request(options, function (error, response, body) {
                        if (error) throw new Error(error);

                        console.log(body);
                        res.json({
                            message: 'New temp created + mail alerte (si pas de mail dans les 30 dernieres minutes)!',
                            data: temp
                        });
                    });
                } else {
                    res.json({
                        message: 'New temp created but no mail alerte!',
                        data: temp
                    });
                }   
            }
        });
    });
};
// Handle view temp info
exports.view = function (req, res) {
    Temp.findById(req.params.temp_id, function (err, temp) {
        if (err)
            res.send(err);
        res.json({
            message: 'Temp details loading..',
            data: temp
        });
    });
};
// Handle update temp info
exports.update = function (req, res) {
    Temp.findById(req.params.temp_id, function (err, temp) {
        if (err)
            res.send(err);
        temp.value = req.body.value;
        // save the temp and check for errors
        temp.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Temp Info updated',
                data: temp
            });
        });
    });
};
// Handle delete temp
exports.delete = function (req, res) {
    Temp.remove({
        _id: req.params.temp_id
    }, function (err, temp) {
        if (err)
            res.send(err);
    res.json({
            status: "success",
            message: 'temp deleted'
        });
    });
};

function recupererSeuilAlerte(cb){
    var options = { 
        method: 'GET',
        url: 'http://localhost:8080/api/seuil',
        headers: { 
            'Content-Type': 'application/json' 
        },
        json: true 
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        cb(body.data.value);
    });
}


