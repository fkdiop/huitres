var request = require("request");
// Import temp model
Seuil = require('./seuilModel');

// Handle index actions
exports.index = function (req, res) {
    Seuil.get(function (err, seuils) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        } else {
            console.log("----> Liste complete seuil: ", seuils);

            res.json({
                status: "success",
                data: seuils[seuils.length-1]
            });
        }
    });
};
// Handle create temp actions
exports.new = function (req, res) {
    console.log("*****> req.body: ", req.body);
    var seuil = new Seuil();
    seuil.value = req.body.value;
    console.log("--------> seuil value: ", seuil.value);
// save the temp and check for errors
    seuil.save(function (err) {
        if (err){
            res.json(err);
        } else {
            res.json({
                message: 'Nouveau seuil créé!',
                data: seuil
            }); 
        }
    });
};
// Handle view temp info
exports.view = function (req, res) {
    Seuil.findById(req.params.seuil_id, function (err, temp) {
        if (err)
            res.send(err);
        res.json({
            message: 'Temp details loading..',
            data: seuil
        });
    });
};
// Handle update temp info
exports.update = function (req, res) {
    Seuil.findById(req.params.seuil_id, function (err, temp) {
        if (err)
            res.send(err);
        temp.value = req.body.value;
        // save the temp and check for errors
        temp.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Temp Info updated',
                data: seuil
            });
        });
    });
};

