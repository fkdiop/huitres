// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'L\'API marche',
        message: 'Bienvenue sur l\'API REST de Fatou!',
    });
});
// Import temp controller
var tempController = require('./tempController');
var mailController = require('./mailController');
var seuilController = require('./seuilController');
// Temp routes
router.route('/temp')
    .get(tempController.index)
    .post(tempController.new);
router.route('/temp/:temp_id')
    .get(tempController.view)
    .patch(tempController.update)
    .put(tempController.update)
    .delete(tempController.delete);
router.route('/email')
	.get(mailController.index)
	.post(mailController.new);
router.route('/seuil')
    .get(seuilController.index)
    .post(seuilController.new);
// Export API routes
module.exports = router;