var nodemailer = require('nodemailer');

// Create the transporter with the required configuration for Outlook
// change the user and pass !
var transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com", // hostname
    secureConnection: false, // TLS requires secureConnection to be false
    port: 587, // port for secure SMTP
    tls: {
       ciphers:'SSLv3'
    },
    auth: {
        user: 'xxxxxxx@hotmail.com',
        pass: 'xxxxxxxxxxxxxxx'
    }
});

// setup e-mail data, even with unicode symbols
var mailOptions = {
    from: '"Alerte Huitres " <xxxxxxxxxx@hotmail.com>', // sender address (who sends)
    to: 'kine.fd@gmail.com', // list of receivers (who receives)
    subject: 'Alerte dépassement température', // Subject line
    text: 'La température du bassin est anormalement élevée.', // plaintext body
    //html: '<b>Hello world </b><br> This is the first email sent with Nodemailer in Node.js' // html body
};

// send mail with defined transport object
transporter.sendMail(mailOptions, function(error, info){
    if(error){
        return console.log(error);
    }
    console.log('Message sent: ' + info.response);
});
