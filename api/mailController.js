var request = require("request");
var nodemailer = require('nodemailer');

const INTERVALLE_ALERTE = 30;

function sendEmail(destinataires, nomCapteur, valeurTemperature){
    // Create the transporter with the required configuration for Outlook
    // change the user and pass !
    var transporter = nodemailer.createTransport({
        host: "smtp-mail.outlook.com", // hostname
        secureConnection: false, // TLS requires secureConnection to be false
        port: 587, // port for secure SMTP
        tls: {
           ciphers:'SSLv3'
        },
        auth: {
            user: 'xxxxxxxxxxx@hotmail.com',
            pass: 'xxxxxxxxxxxxx'
        }
    });

    // setup e-mail data, even with unicode symbols
    var mailOptions = {
        from: '"Alerte Huitres " <xxxxxxxxx@hotmail.com>', // sender address (who sends)
        to: destinataires, // list of receivers (who receives)
        subject: 'Alerte dépassement température', // Subject line
        text: 'Le capteur '+ nomCapteur+' a détecté une température de '+ valeurTemperature +'', // plaintext body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
}

function lastEmailSent(cb){
    var options = { 
        method: 'GET',
        url: 'http://localhost:8080/api/email',
        headers: { 'Content-Type': 'application/json' },
        json: true 
    };

    request(options, function (error, response, body) {
        if (error) throw new Error(error);

        console.log(body.data);
        cb(body.data);
    });
}

// Import temp model
Mail = require('./mailModel');
// Handle index actions
exports.index = function (req, res) {
    Mail.get(function (err, mails) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        } else {
            res.json({
                status: "success",
                message: "Temps retrieved successfully",
                data: mails[mails.length-1]
            });
        }
    });
};
// Handle create temp actions
exports.new = function (req, res) {
    var mail = new Mail();
    mail.value = req.body.value;
    mail.destinataires = req.body.destinataires;
    mail.nomCapteur = req.body.nomCapteur;
    mail.valeurTemperature = req.body.valeurTemperature;

    lastEmailSent(function(data){
        if(data !== undefined){
            console.log('-----> lastEmailSent: ', data.create_date);
            var diffMs = Date.now() - new Date(data.create_date);
            // var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
            var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
            console.log('++++> Difference: ', diffMins);

            if(diffMins > INTERVALLE_ALERTE){
                sendEmail(mail.destinataires, mail.nomCapteur, mail.valeurTemperature);
                    // save the temp and check for errors
                mail.save(function (err) {
                    if (err){
                        res.json(err);
                    } else {
                        res.json({
                            message: 'New mail sent + created!',
                            data: mail
                        });
                    }
                });
            } else {
                console.log('********* Alerte envoyée moins de 30 mn avant **********');
                res.json({
                    message: 'No mail sent + no mail created!',
                    data: mail
                });
            }
        } else {
            sendEmail(mail.destinataires, mail.nomCapteur, mail.valeurTemperature);
            // save the temp and check for errors
            mail.save(function (err) {
                if (err){
                    res.json(err);
                } else {
                    res.json({
                        message: 'New mail sent + created!',
                        data: mail
                    });
                }
            });
        }
    });
};
// Handle view temp info
exports.view = function (req, res) {
    Mail.findById(req.params.mail_id, function (err, mail) {
        if (err)
            res.send(err);
        res.json({
            message: 'Temp details loading..',
            data: mail
        });
    });
};
// Handle update temp info
exports.update = function (req, res) {
    Mail.findById(req.params.mail_id, function (err, mail) {
        if (err)
            res.send(err);
        mail.value = req.body.value;
        // save the temp and check for errors
        mail.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Temp Info updated',
                data: mail
            });
        });
    });
};
// Handle delete temp
exports.delete = function (req, res) {
    Mail.remove({
        _id: req.params.mail_id
    }, function (err, mail) {
        if (err)
            res.send(err);
    res.json({
            status: "success",
            message: 'temp deleted'
        });
    });
};
