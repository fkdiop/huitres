var mongoose = require('mongoose');
// Setup schema
var mailSchema = mongoose.Schema({
    value: {
        type: String,
        required: true
    },
    destinataires: {
        type: String,
        required: true
    },
    nomCapteur: {
        type: String,
        required: false
    },
    valeurTemperature: {
        type: String,
        required: false
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Temp model
var Mail = module.exports = mongoose.model('mail', mailSchema);
module.exports.get = function (callback, limit) {
    Mail.find(callback).limit(limit);
}