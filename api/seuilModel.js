var mongoose = require('mongoose');
// Setup schema
var seuilSchema = mongoose.Schema({
    value: {
        type: Number,
        required: true
    },
    create_date: {
        type: Date,
        default: Date.now
    }
});
// Export Temp model
var Seuil = module.exports = mongoose.model('seuil', seuilSchema);
module.exports.get = function (callback, limit) {
    Seuil.find(callback).limit(limit);
}