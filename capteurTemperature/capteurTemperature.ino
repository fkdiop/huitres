#include <ESP8266WiFi.h>
#include <WiFiClient.h> 
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// Data wire is plugged into port D2 on the ESP8266
#define ONE_WIRE_BUS D2
// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

// variable to hold device addresses
DeviceAddress Thermometer;

int deviceCount = 0;

float tempSensor1, tempSensor2, tempSensor3;

uint8_t sensor1[8] = { 0x28, 0xAA, 0x8C, 0xC0, 0x52, 0x14, 0x01, 0xE3 };
uint8_t sensor2[8] = { 0x28, 0xAA, 0xC1, 0xC1, 0x52, 0x14, 0x01, 0x69 };

/* Set these to your desired credentials. */
const char *ssid = "xxxxxxx";  //ENTER YOUR WIFI SETTINGS
const char *password = "xxxxxxx";
 
//Web/Server address to read/write from 
const char *host = "51.38.32.33"; // IP address of server
 
//=======================================================================
//                    Power on setup
//=======================================================================
 
void setup() {
  delay(1000);
  Serial.begin(115200);
  WiFi.mode(WIFI_OFF);        //Prevents reconnection issue (taking too long to connect)
  delay(1000);
  WiFi.mode(WIFI_STA);        //This line hides the viewing of ESP as wifi hotspot
  
  WiFi.begin(ssid, password);     //Connect to your WiFi router
  Serial.println("");
 
  Serial.print("Connecting");
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
 
  //If connection successful show IP address in serial monitor
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());  //IP address assigned to your ESP
}

//=======================================================================
//                    Main Program Loop
//=======================================================================
void loop() {
  sensors.requestTemperatures();
  tempSensor1 = sensors.getTempC(sensor1); 
  tempSensor2 = sensors.getTempC(sensor2); 

  Serial.print("Temperature - 1: ");
  Serial.println(tempSensor1); 
  Serial.print("Temperature - 2: ");
  Serial.println(tempSensor2);
  
  HTTPClient http1;    //Declare object of class HTTPClient
  //Post Data
  String postData1 = "{\"name\": \"capteur_temp_1\", \"value\":"+ String(tempSensor1) +"}";
  http1.begin("http://51.38.32.33:8080/api/temp");              //Specify request destination
  http1.addHeader("Content-Type", "application/json");    //Specify content-type header
  int httpCode1 = http1.POST(postData1);   //Send the request
  String payload1 = http1.getString();    //Get the response payload
  Serial.println(httpCode1);   //Print HTTP return code
  Serial.println(payload1);    //Print request response payload
  http1.end();  //Close connection

  HTTPClient http2;    //Declare object of class HTTPClient
  //Post Data
  String postData2 = "{\"name\": \"capteur_temp_2\", \"value\":"+ String(tempSensor2) +"}";
  http2.begin("http://51.38.32.33:8080/api/temp");              //Specify request destination
  http2.addHeader("Content-Type", "application/json");    //Specify content-type header
  int httpCode2 = http2.POST(postData2);   //Send the request
  String payload2 = http2.getString();    //Get the response payload
  Serial.println(httpCode2);   //Print HTTP return code
  Serial.println(payload2);    //Print request response payload
  http2.end();  //Close connection
  
  delay(600000);  //Post Data at every 600 seconds
}
