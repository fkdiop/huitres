//Fonction pour la récupération des températures
function recupererTemperatures(){
  $.ajax({
    type:"GET", 
    url: "http://51.38.32.33:8080/api/temp", 
    //url: "http://localhost:8080/api/temp",
    success: function(response) {
      //console.log('Data: ', response.data);
      recupererSeuilAlerte(function(seuilAlerte){
        tracerGraphe(response.data, seuilAlerte);
      });
    }, 
    error: function(jqXHR, textStatus, errorThrown) {
      console.log('Error: ', errorThrown);
    }
  });
}

//Afficher et raffraichir à intervalle régulier les données 
//de température et de seuil d'alerte
$(document).ready(function(){
  recupererTemperatures();
  recupererDernierEmailEnvoye();
  setInterval(function(){
    recupererTemperatures();
    recupererDernierEmailEnvoye();
  }, 60000);
});

//Fonction permettant de tracer le graphe de suivi des températures
//et du seuil d'alerte
function tracerGraphe(data, seuilAlerte) {
  $('#chart').highcharts({
    xAxis: {
        type: 'datetime',
        title: {
           text: 'Date de relevé'
        }
    },
    yAxis: {
      title: {
          text: 'Températures (°C)'
      },
      plotLines:[{
        value: seuilAlerte,
        color: '#ff0000',
        width:5,
        zIndex:4,
        label:{text:'Seuil Alerte'}
      }]
    },
    title: {
      text: 'Courbe de températures du bassin'
    },
    exporting: {
      enabled: true
    },
    credits: {
      enabled: false
    },
    series: data
  });
}

//Fontion pour récupére et afficher le dernier mail envoyé
//pendant les 30 dernières minutes
function recupererDernierEmailEnvoye(){
 $.ajax({
    type:"GET",
    url: "http://51.38.32.33:8080/api/email",
    //url: "http://localhost:8080/api/email",
    success: function(response) {
      dateLastEmailSent = response.data.create_date;
      var diffMs = new Date(Date.now()) - new Date(dateLastEmailSent);
      seconds = Math.floor(diffMs / 1000);
      minutes = Math.floor(seconds / 60);
      //console.log("******> Minutes: ", minutes);

      if(minutes < 30){
        const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric' };
        dateLastEmailSent = new Date(dateLastEmailSent);
        //console.log("----> dateLastEmailSent: ", dateLastEmailSent.toLocaleDateString("fr-FR", options));
        $("#mailAlert").show();
        $("#textAlert" ).html("Un mail d'alerte a déjà été envoyé à : <strong>" + dateLastEmailSent.toLocaleDateString("fr-FR", options) + "</strong>");
      }
    },
    error: function(jqXHR, textStatus, errorThrown) {
      console.log('Error: ', errorThrown);
    }
  });
}

//Fonction pour récuperer le seuil d'alerte à travers une requete GET
function recupererSeuilAlerte(cb){
  $.ajax({
    type:"GET", 
    url: "http://51.38.32.33:8080/api/seuil", 
    // url: "http://localhost:8080/api/seuil",
    success: function(response) {
      console.log('Data: ', response.data);
      cb(response.data.value);
     //$("#seuilAlerte").html(response.data.value + " °C");
    }, 
    error: function(jqXHR, textStatus, errorThrown) {
      console.log('Error: ', errorThrown);
    }
  });
}

//Fonction pour afficher le seuil d'alerte
//et raffraichir le graphe 
function afficherSeuilAlerte(){
  recupererSeuilAlerte(function(seuilAlerte){
    $("#seuilAlerte").html(seuilAlerte + " °C");
    recupererTemperatures()
  });
}
afficherSeuilAlerte();

//Function pour envoyer le nouveau seuil entré par l'utilisateur
//dans le champ input dédié
function envoyerNouveauSeuil(){
  var nouveauSeuil = $("#nouveauSeuil").val();
    console.log("Nouvelle valeur envoyée!!! ", nouveauSeuil);
  $.ajax({
    type:"POST", 
    url: "http://51.38.32.33:8080/api/seuil", 
    //url: "http://localhost:8080/api/seuil",
    data: {'value': nouveauSeuil },
    dataType:'json',
    success: function(response) {
      console.log('Creation nouveau seuil: ', response);
      afficherSeuilAlerte();
    }, 
    error: function(jqXHR, textStatus, errorThrown) {
      console.log('Error: ', errorThrown);
    }
  });
}